var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Items
var Coin = new Schema({
  Username: {
    type: String
  },
  Email: {
    type: String
  },
  Password: {
    type: String
  },
  Mobile: {
    type: Number
  },
},{
    collection: 'Assets_and_data'
});

module.exports = mongoose.model('Coin', Coin);