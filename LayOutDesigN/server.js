

const express = require('express'),
      path = require('path'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      mongoose = require('mongoose'),
      config = require('./config/DB'),
      coinRoutes = require('./expressRoutes/coinRoutes');


      var gridfs = require('gridfs-stream');
      var fs = require('fs');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
  );


  gridfs.mongo = mongoose.mongo;



const app = express();
app.use(bodyParser.json());
app.use(cors());
const port = process.env.PORT || 4000;

app.use('/Assets_and_data', coinRoutes);
app.use('/Filedata', coinRoutes);


const server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});