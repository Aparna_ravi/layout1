import { Component, OnInit } from '@angular/core';
import {  RegisterService } from '../../../register.service'
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
// import { REACTIVE_FORM_DIRECTIVES } from '@angular/forms'
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  res:any;
  angForm: FormGroup;
  constructor(private registerservice:  RegisterService, private fb: FormBuilder,private AuthService:AuthService) {
    this.createForm();

   }
 
  createForm() {
    this.angForm = this.fb.group({
      Username: ['', Validators.required ],
     Email: ['', Validators.required ],
     Password: ['', Validators.required ],
     Mobile: ['', Validators.required ]


   });
  }
  addCoin(Username,Email,Password,Mobile) {
    
  this.res=this.AuthService.us;
    console.log(this.res)
    const re =this.res.find((u)=>{
      return u.Email=== Email;
    });
    if(re)
    {
    alert("already registered mail");
    }
    else{
      this.registerservice.addCoin(Username,Email,Password,Mobile);

    }


    // this.registerservice.addCoin(Username,Email,Password,Mobile);
}
  
  ngOnInit() {
  }

}
