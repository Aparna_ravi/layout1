import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from '../../../register.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
logs:boolean
user:any;
  constructor(private service:AuthService,
  private router :Router,private http: HttpClient) {
    this.logs=this.service.log;
    console.log(this.service.log)
   }
  onLoginSubmit(myForm:NgForm)
  {
    if(myForm.valid)
    {
    this.service.login(myForm.value);
    }
    else{
    this.logs=this.service.log;

      console.log(myForm.value);
      console.log("error");

    }
  }
  ngOnInit() {
   
  }
  


setlog()
{
  this.logs=this.service.log;

}
  register()
{
  console.log("register")
  this.router.navigate(['/reg']);
}
}
