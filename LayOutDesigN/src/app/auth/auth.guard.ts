import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
 import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth:AuthService,
  private router:Router)
  {
    
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const isLoggedin=this.auth.isLoggedIn();
      if(this.auth.logg)
    return true;


    else{
      this.router.navigate(['/login']);
      return false;
    }
 

  }
// canActivate( route : ActivatedRouteSnapshot, state : RouterStateSnapshot ) {
//     if(this.auth.logg) 
//     {
//         console.log("login");
//         return true;
//     }
//     else{
//         console.log("no login");
//     }
//     // else navigate to login
// }
}
