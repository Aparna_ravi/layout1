import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {  RegisterService } from '../../../register.service'
import { AuthService } from '../../../auth/auth.service';
import { Router } from '../../../../../node_modules/@angular/router';
@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
  angForm: FormGroup;
  Usermail:String;
  fname:any;
  public show :boolean=false;
  constructor(private registerservice:  RegisterService, private router:Router,private fb: FormBuilder,private AuthService:AuthService) {
    this.createForm();
   }
q=this.AuthService.currentmail;
  createForm() {
    this.angForm = this.fb.group({
      Country: ['', Validators.required ],
      BusinessSegment: ['', Validators.required ],
      Brand: ['', Validators.required ]


   });
  }
toggleShow()
{
  this.show=!this.show;
}


ext:string;

onChange(event: EventTarget) {
  let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
  let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
  let files: FileList = target.files;
  this.fname = files[0].name;
  this.ext=this.fname.split('.')[1]
  console.log(this.fname);
  console.log(this.ext);


}
imge:boolean
vid:boolean

pdf:boolean


  addmetadata(Country,BusinessSegment,Brand) {
   
    
      
    this.Usermail= this.AuthService.currentmail;
        this.registerservice.addmetadata(Country,BusinessSegment,Brand,this.Usermail,"assets/media/"+this.fname,this.fname);
    
   

    }
  
      // this.registerservice.addCoin(Username,Email,Password,Mobile);
  



 
  up()
  {
    // this.router.navigateByUrl('/RefrshComponent', {skipLocationChange: true}).then(()=>
this.router.navigate(["/layout"]);
window.location.reload();

  }
 
  

  ngOnInit() {

  }

}
