import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute} from '../../../../../node_modules/@angular/router';


@Component({
  selector: 'app-newwindow',
  templateUrl: './newwindow.component.html',
  styleUrls: ['./newwindow.component.css']
})
export class NewwindowComponent implements OnInit {
val;
splitArr:String[]=[];
s;
  constructor( private router:Router , private url : ActivatedRoute) { }

  ngOnInit() {
    let myImg = this.url.snapshot.paramMap.get('data').split('-').join('/');
    // console.log(myImg);
    
    this.s=myImg.split('.')[1]
    this.fun();
    
    
    
    this.val=myImg;
  }

fun()
{
  if (this.s === "jpg" || this.s === "png")
  return "imgg";
else if (this.s === "mp4")
  return "vid";
else {
  return "pdf";
}
}

}
