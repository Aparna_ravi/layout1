import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import { LoginComponent } from './auth/components/login/login.component';
import { LayoutComponent } from './dashboard/components/layout/layout.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { FileuploadComponent } from './dashboard/components/fileupload/fileupload.component';
import { NewwindowComponent } from './dashboard/components/newwindow/newwindow.component';


const routes:Routes =[
  {
    path:'login',
    component: LoginComponent
  },
 
  {
    path:'layout',
     component:LayoutComponent,
     canActivate: [AuthGuard],
//  runGuardsAndResolvers: 'always'

  },
  {
    path:'reg',
     component:RegisterComponent
     

  },
  {
    path:'open/:data',
     component:NewwindowComponent,
     canActivate: [AuthGuard],

     

  },
 
{
path:'upload',

 component: FileuploadComponent,
 canActivate: [AuthGuard],
//  runGuardsAndResolvers: 'always'
},
  {
  path:'',
  redirectTo:'/login',
  pathMatch:'full'
  }  
]
@NgModule({
  imports: [   
    RouterModule.forRoot(routes)
    // RouterModule.forRoot(routes,{onSameUrlNavigation:'reload'})


  ],
  exports :[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
