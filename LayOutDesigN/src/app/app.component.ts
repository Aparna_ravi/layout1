import { Component, OnInit } from '@angular/core';
import {AppserviceService } from './appservice.service';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  activeView='productList';
  isLoggedIn;
  constructor(private service :AppserviceService,
     private authservice :AuthService  ) { }
 
  ngOnInit()
  {
    if(localStorage.getItem('UserEmail')!=null)
    {
      alert("hellooo")
     this.isLoggedIn=true;
    
    }
    // this.service.viewChangeEvent.subscribe(viewName =>
    // {
    //   this.activeView=viewName;
    // });
    this.isLoggedIn=this.authservice.isLoggedIn();
    this.authservice.loginChangeEvent.subscribe(isLoggedIn=>{
      this.isLoggedIn=isLoggedIn;
    });
  }
  onLogout()
  {
    this.authservice.logout();
    this.isLoggedIn="";
  }
  // setView(viewName)
  // {
  //   this.activeView=viewName;
  // }
}
