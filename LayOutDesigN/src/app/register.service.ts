import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  check:any;
  result: any;
  data:any;
  constructor(private http: HttpClient) { }
 
  addCoin(Username,Email,Password,Mobile) {
  
    
   
    // console.log(Username);
    const uri = 'http://localhost:4000/Assets_and_data/add';
    const obj = {
      Username:Username,
      Email:Email,
      Password:Password,
      Mobile:Mobile

    };
    this
      .http
      .post(uri, obj)
      .subscribe(res =>
          console.log('Done'));
  }


  addmetadata(Country,BusinessSegment,Brand,Usermail,Filename,name) {
  
    
   
    const uri = 'http://localhost:4000/Filedata/addi';
    const obj = {
      Country:Country,
      BusinessSegment:BusinessSegment,
      Brand:Brand,
      Usermail:Usermail,
      Filename:Filename,
      name:name,

    

    };
   console.log(obj.Country);

    this
      .http
      .post(uri, obj)
      .subscribe(res =>
          console.log('Done'));
  }
  getCoins():Observable<any> {
    const uri = 'http://localhost:4000/Assets_and_data';
    return this
            .http
            .get(uri)
            .pipe(
            map(res => {
              return res;
            })
          );
  }

  getmetadata():Observable<any> {
    const uri = 'http://localhost:4000/Filedata/getmet';
    return this
            .http
            .get(uri)
            .pipe(
            map(res => {
              return res;
            })
          );
  }

  editCoin(id) {
    const uri = 'http://localhost:4000/Assets_and_data/edit/' + id;
    return this
            .http
            .get(uri)
            .pipe(
            map(res => {
              return res;
            })
          );
  }
  updateCoin(name, price, id) {
    const uri = 'http://localhost:4000/Assets_and_data/update/' + id;

    const obj = {
      name: name,
      price: price
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res => console.log('Done'));
  }
  
  deleteCoin(id) {
    const uri = 'http://localhost:4000/Assets_and_data/delete/' + id;

        return this
            .http
            .get(uri)
            .pipe(
            map(res => {
              return res;
            })
          );
  }
}
